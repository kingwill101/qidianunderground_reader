import 'util.dart' as util;
import 'package:privatebin_reader/privatebin_reader.dart';
import 'dart:async';
import 'package:qiutils/src/utils/general/general.dart';
import 'package:sqflite/sqflite.dart';

Future<String> fetchPrivateBin(String url) async {
  Completer c = Completer<String>();

  util.fetchPrivateBin(url).then((PrivateBinInfo pi) async {
    print("Decrypting");
    var val = await PrivatebinReader.decrypt(pi.data, pi.password);
    print("decompressing");
    var d = await PrivatebinReader.decompress(val);
    print("all done");
    c.complete(util.cleanString(d));
  }).catchError((err) {
    c.completeError(err);
  });
  return c.future;
}

class FlutterStorage implements util.StorageClass {
  Future<Database> getDatabase() async {
    String dpath = await getDatabasesPath();
    String path = "$dpath/qiutils/src.db";
    Database database = await openDatabase(path, version: 1,
        onCreate: (Database _db, int version) async {
      // When creating the db, create the table
      await _db.execute(
          'CREATE TABLE IF NOT EXISTS saved (id INTEGER PRIMARY KEY, name TEXT, value INTEGER, num REAL)');
      await _db.execute(
          'CREATE TABLE IF NOT EXISTS Test (id INTEGER PRIMARY KEY, name TEXT, value INTEGER, num REAL)');
      await _db.execute(
          'CREATE TABLE IF NOT EXISTS search (id INTEGER PRIMARY KEY, query TEXT)');
      await _db.execute(
          'CREATE TABLE IF NOT EXISTS hidden (id INTEGER PRIMARY KEY, name TEXT, uri TEXT)');
    });
    return database;
  }

  @override
  T getItem<T>(String name) {
    return null;
  }

  @override
  void setItem(String name, String val) {}

  Future<List<Map<String, dynamic>>> getSearchResults() async{
    Completer<List<Map<String, dynamic>>> c = Completer();
     getDatabase().then((Database d){
       c.complete(d.rawQuery("SELECT * FROM search"));
     });
  return c.future;
  }

  Future<List<Map<String, dynamic>>> getHidden() async{
    Completer<List<Map<String, dynamic>>> c = Completer();
     getDatabase().then((Database d){
       c.complete(d.rawQuery("SELECT * FROM hidden"));
     });
  return c.future;
  }

  Stream<void> addHidden(String name, String uri) {
    Completer<Stream<void>> c = Completer<Stream<void>>();

    getDatabase().then((Database d) {
      c.complete(
          d.execute("INSERT INTO hidden(name, uri) VALUES(?,?)", [name, uri]).asStream());
    });
    return c.future.asStream();
  }

  Stream<void> saveSearchQuery(String query) {
    Completer<Stream<void>> c = Completer<Stream<void>>();

    getDatabase().then((Database d) {
      c.complete(
          d.execute("INSERT INTO search(query) VALUES(?)", [query]).asStream());
    });
    return c.future.asStream();
  }
}
