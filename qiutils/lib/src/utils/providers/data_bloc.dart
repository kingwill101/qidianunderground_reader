import 'package:qiutils/src/utils/util.dart';
import 'package:qiutils/src/utils/general/types.dart';
import 'dart:async';
import 'package:rxdart/rxdart.dart' show BehaviorSubject;

class FeedBloc {
  Redditlist _homeFeed = Redditlist();

  final _sortTypeController = StreamController<FeedType>();

  final _postsSubject = BehaviorSubject<List<RedditPost>>();

  final _sortTypeSubject = BehaviorSubject<FeedType>(seedValue: FeedType.New);

  final _isloadingSubject = BehaviorSubject<bool>();

  FeedBloc() {
    _sortTypeController.stream.listen((FeedType ft) {
      if (getFeedType(ft) == getFeedType(_sortTypeSubject.value)) {
        print("already using - $ft");
        return;
      }
      if (ft != null) {
        _sortTypeSubject.add(ft);
        _fetchFeed(ft);
      }
    });
  }

  _fetchFeed(ft) async {
    _isloadingSubject.add(true);
    fetchCategory(getFeedType(_sortTypeSubject.value).toLowerCase()).then((r) {
      _homeFeed = r;
      _postsSubject.add(_homeFeed.posts);
    }).catchError((err) {
      _postsSubject.addError(err);
    });
    _isloadingSubject.add(false);
  }

  Stream<List<RedditPost>> get posts => _postsSubject.stream;

  Sink<FeedType> get sortType => _sortTypeController.sink;

  Stream<FeedType> get getStream => _sortTypeSubject.stream;

  Stream<bool> get isLoading => _isloadingSubject.stream;

  void cleanup() {
    _sortTypeSubject.close();
    _isloadingSubject.close();
    _sortTypeController.close();
  }
}
