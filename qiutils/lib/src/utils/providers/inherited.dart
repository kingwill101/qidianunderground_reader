import 'package:flutter/material.dart';
import 'package:qiutils/src/utils/providers/data_bloc.dart';

class HomeProvider extends InheritedWidget {
  final FeedBloc bloc;

  HomeProvider(this.bloc, {Widget child}) : super(child:child);
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  static FeedBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(HomeProvider) as HomeProvider)
        .bloc;
  }
}
