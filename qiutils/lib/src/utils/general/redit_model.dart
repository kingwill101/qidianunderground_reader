import 'dart:convert';


class RedditPost {
  final String  url;
  final String  title;
  final String  authorFullname;
  final double  createdUtc;
  final String linkFlareText;

  RedditPost({
    this.url,
    this.title,
    this.authorFullname,
    this.createdUtc,
    this.linkFlareText
  });

  Map<String, dynamic> toMap() => {
    "data":{
      "url": this?.url, 
      "title": this?.title,
      "author_fullname":this?.authorFullname,
      "created_utc": this?.createdUtc,
      "link_flare_text": this?.linkFlareText
    }
  };


  String toJSON(){
    return json.encode(this.toMap());
  }

  RedditPost.fromMap(Map<String, dynamic> m)
  : url =m["data"]["url"],
  title = m["data"]["title"],
  authorFullname = m["data"]["author_fullname"],
  createdUtc =m["data"]["created_utc"],
  linkFlareText =m["data"]["link_flare_text"];
}

class Redditlist {
  List<RedditPost> posts =[];
  String before;
  String after;

  Redditlist({this.posts, this.before, this.after});

  String toJson(){
    return json.encode(this.toMap());
  }

  Map<String, dynamic> toMap(){

    return {
      "data": {
        "before":this.before,
        "after": this.after,
        "children": this.posts.map((p) => p.toMap())
      },
    };
  }

  factory Redditlist.fromJson(String source) { 

    var m = json.decode(source);
    
    String _before = m['data']['before'];
    String _after = m['data']['after'];
    var data = m["data"]["children"];
    List<RedditPost> p = [];
    data.forEach((m){
      p.add(RedditPost.fromMap(m));
    });   

    return new Redditlist(posts:p, before:_before, after:_after);
  }
}
