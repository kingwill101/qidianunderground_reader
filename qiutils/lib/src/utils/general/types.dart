export 'redit_model.dart';

class Section {
  String name;
  List<Listing> listing = [];
}

class Listing {
  String name;
  List<Entry> entries = [];
}

class Entry {
  String name;
  String url;

  Entry(this.name, this.url);

  @override
  String toString() {
    return "${this.name} - ${this.url}";
  }
}

class PrivateBinInfo{
  final String data;

  final String password;

  PrivateBinInfo(this.data, this.password);
}


enum FeedType { Hot, New, Top, Controversial }

String getFeedType(FeedType ft) {
  switch (ft) {
    case FeedType.Controversial:
      return "Controversial";

    case FeedType.Hot:
      return "Hot";

    case FeedType.New:
      return "New";

    case FeedType.Top:
      return "Top";
    default:
      return "";
  }
}


class Configuration {
  bool darkMode;
  Configuration({this.darkMode=false});
}