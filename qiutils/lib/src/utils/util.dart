import 'package:html_unescape/html_unescape.dart';
import 'package:html/parser.dart';
import 'package:html/dom.dart';
import 'dart:convert';
import 'package:http/http.dart';
import 'dart:async';
import 'general/types.dart';

class HtmlFixResult {
  List<String> keys = [];
  String data;

  HtmlFixResult(this.keys, this.data);
}

HtmlFixResult fixHTML(String data) {
  String res = HtmlUnescape().convert(data).toString();

  List<String> keysFound = [];
  for (var i = 0; i < 26; i++) {
    int currentKeyCode = 97 + i;

    String currKey = String.fromCharCode(97 + i);
    String patternBefore = '<h5 id="wiki_$currKey">';
    String patternAfter = '<div id="wiki__$currKey"> <h5 id="wiki_$currKey">';

    if (currentKeyCode > 97) {
      if (currentKeyCode < 122) {
        patternAfter =
            '</div> <div id="wiki__$currKey"> <h5 id="wiki_$currKey">';
      }
    }

    if (res.contains(patternBefore) == true) {
      res = res.replaceAll(patternBefore, patternAfter);
      keysFound.add("wiki__$currKey");
    }
  }
  res = res + "</div>";

  print("================\n\n $res \n\n==================");
  return HtmlFixResult(keysFound, res);
}

Future<List<Section>> buildListing(String data) async {
  HtmlFixResult res = fixHTML(data);
  Document doc = parse(res.data);
  List<Section> sections = [];
  res.keys.forEach((String key) {
    Section section = Section();

    Element el = doc.getElementById(key);
    if (el != null) {
      var sectionHeading = el.getElementsByTagName("h5")[0].text;

      var li = el.getElementsByTagName("ul");
      var name = el.getElementsByTagName("p");

      section.name = sectionHeading;

      assert(li.length == name.length);

      for (var i = 0; i < li.length; i++) {
        Listing _listing = Listing();
        _listing.name = name[i].text;

        li[i].children[0].children.forEach((Element e) {
          String url = e.attributes.containsKey("href")
              ? e.attributes["href"]
              : "No URL HERE";
          _listing.entries.add(Entry(e.text, url));
        });

        section.listing.add(_listing);
      }
    }
    sections.add(section);
  });

  return sections;
}

Future<String> getQIndex() async {
  Completer c = Completer<String>();

//  reddit.Reddit r = reddit.Reddit(Client());
//
//  r.

  get("https://www.reddit.com/r/QidianUnderground/wiki/toc_reup.json")
      .then((Response res) {
    try {
      var bod = jsonDecode(res.body);
      String data = bod['data']['content_html'];
      c.complete(data);
    } catch (e) {
      c.completeError(e);
    }
  });

  return c.future;
}

Future<String> getToc() async {
  Completer c = Completer<String>();
  get("https://www.reddit.com/r/QidianUnderground/wiki/autotoc.json")
      .then((Response res) {
    try {
      print(res.body);

      var bod = jsonDecode(res.body);
      String data = bod['data']['content_html'];
      c.complete(data);
    } catch (e) {
      c.completeError(e);
    }
  });

  return c.future;
}

Future<Redditlist> searchQU(String query) {
  Completer<Redditlist> c = Completer<Redditlist>();

  Uri outgoingUri = new Uri(
      scheme: 'https',
      host: 'reddit.com',
      path: 'r/QidianUnderground/search.json',
      queryParameters: {"q": query, "restrict_sr": "1"});
  get(outgoingUri).then((Response r) {
    try {
      Redditlist res = Redditlist.fromJson(r.body);

      c.complete(res);
    } catch (er) {
      print("Error parsing - > $er");
      c.completeError(er);
    }
  }).catchError((err) {
    c.completeError(err);
  });

  return c.future;
}

Future<Redditlist> fetchCategory(String cat) {
  Completer<Redditlist> c = Completer();
  print("Category - > $cat");

  String url = "https://www.reddit.com/r/QidianUnderground/$cat.json";

  get(url).then((Response r) {
    Redditlist res = Redditlist.fromJson(r.body);
    
     c.complete(res);
  }).catchError((e) {
    c.completeError(e);
  });

  return c.future;
}

List<dynamic> filter(List<dynamic> res){
List<dynamic>_res =[];
     
    res.forEach((it){


        if (it["data"]["url"].contains("reddit.com")){
          return;
        }
        if (it["data"]["url"].contains("redd.it")){
          return;
        }
        // print("allowing ${it["data"]["title"]} ${it["data"]["url"]}");
        _res.add(it);
    });

    return _res;
}

Future<PrivateBinInfo> fetchPrivateBin(String url) {
  Completer c = Completer<PrivateBinInfo>();

  String pw = url.split("#")[1];

  get("$url&json", headers: {'X-Requested-With': 'JSONHttpRequest'})
      .then((Response r) async {
    c.complete(PrivateBinInfo(json.decode(r.body)['data'], pw));
  }).catchError((Object err) {
    print("error fetching $url ${err.toString()}");
    c.completeError(err);
  });

  return c.future;
}

String cleanString(String _in) {
  return HtmlUnescape().convert(_in).toString();
}


abstract class StorageClass {
  void setItem(String name, String val);
  T getItem<T>(String name);
}
