import 'package:flutter/material.dart';

class NetworkReloadWidget extends StatelessWidget {
  final String _message;
  final Future<void> Function() _reloadFunc;

  NetworkReloadWidget(this._message, this._reloadFunc);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            _message,
            textAlign: TextAlign.center,
          ),
          FlatButton(
            child: Text("Retry"),
            onPressed: () {
              Future.delayed(Duration(milliseconds: 500)).then((v) {
                _reloadFunc();
              });
            },
          )
        ],
      ),
    );
  }
}
