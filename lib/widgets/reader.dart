import 'package:flutter/material.dart';
import 'package:qiunderground/utils/blocs/data_bloc.dart';
import 'package:qiunderground/utils/blocs/toc_bloc.dart';
import 'package:qiunderground/utils/http/http_flutter.dart' as http;
import 'package:qiunderground/utils/general/types.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:qiunderground/utils/utils_flutter.dart';
import 'package:qiunderground/widgets/network_reload_widget.dart';

SafeArea _buildReaderView(BuildContext context, String title, String results) {
  return new SafeArea(
      child: Column(
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(left: 20, top: 5),
        alignment: Alignment.centerLeft,
        child: Text(
          title,
          textAlign: TextAlign.left,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Theme.of(context).textTheme.title.color),
        ),
      ),
      Expanded(
        child: ContentViewWidget(results),
      )
    ],
  ));
}

class ContentViewWidget extends StatelessWidget {
  final String results;

  ContentViewWidget(this.results);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10.0, left: 5, right: 5),
        child: Markdown(
          data: this.results == null ? "" : this.results,
        ));
  }
}

class SingleContentWidget extends StatefulWidget {
  final url;
  final title;
  final FeedBloc bloc;

  SingleContentWidget(this.bloc, this.title, this.url);

  @override
  SingleContentWidgetState createState() {
    bloc.currentArticleUrl.add(this.url);
    bloc.updateCurrentArticle();
    return SingleContentWidgetState();
  }
}

class SingleContentWidgetState extends State<SingleContentWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StreamBuilder<String>(
          stream: widget.bloc.currentArticle,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasError) {
              return NetworkReloadWidget("Error loading article. click to reload",
                  widget.bloc.updateCurrentArticle);
            }

            if (snapshot.hasData) {
              return _buildReaderView(context, widget.title, snapshot.data);
            }

            return Container(
              width: 0,
              height: 0,
            );
          },
        ));
  }
}


class BookViewWidget extends StatefulWidget {
  final Listing listing;
  final String title;
  final TOCBloc bloc;

  BookViewWidget(this.bloc, this.listing, {this.title}) {
    bloc.setCurrentListing(this.listing);
  }

  @override
  BookViewWidgetState createState() {
    return BookViewWidgetState();
  }
}

class BookViewWidgetState extends State<BookViewWidget> {
  bool loadingArticle = true;

//  Entry _currentEntry;
  String currentResults;
  String selectedSideEntry;

  void changeArticle(Entry e, {int index}) {
    Navigator.pop(context);
    if (index != null) {
      widget.bloc.entryIndexChanger.add(index);
    } else {
      widget.bloc.entryIndexChanger.add(0);
    }
//    selectedSideEntry = e.name;
//    Navigator.pop(context);
//
//    setState(() {
//      loadingArticle = true;
//    });
//
//    fetchResults(e);
  }

  @override
  void initState() {
    widget.bloc.entryIndexChanger.add(0);
//    if (_currentEntry == null) {
//      _currentEntry = widget.listing.entries[0];
//      this.selectedSideEntry = _currentEntry.name;
//      loadingArticle = true;
//      fetchResults(_currentEntry);
//    }

    super.initState();
  }

  Widget _buildDrawerContent() {
    return StreamBuilder<Listing>(
      stream: widget.bloc.listing,
      builder: (BuildContext context, AsyncSnapshot<Listing> snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
              itemCount: snapshot.data.entries.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(
                    snapshot.data.entries[index].name,
                    style: TextStyle(
                        color: widget.bloc.currentEntry.name ==
                                snapshot.data.entries[index].name
                            ? Colors.white70
                            : Colors.white),
                  ),
                  onTap: () => widget.bloc.currentEntry.name ==
                          snapshot.data.entries[index].name
                      ? Navigator.pop(context)
                      : changeArticle(snapshot.data.entries[index],
                          index: index),
                  selected: widget.bloc.currentEntry.name ==
                          snapshot.data.entries[index].name
                      ? true
                      : false,
                );
              });
        } else {
          return Container();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: new Container(
          decoration: new BoxDecoration(color: Colors.black87),
          child: Column(
            children: <Widget>[
              Container(
                child: Padding(
                    padding: EdgeInsets.only(left: 30, bottom: 10),
                    child: Align(
                      child: Text(
                        "Contents",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                        ),
                      ),
                      alignment: Alignment.centerLeft,
                    )),
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).padding.top + 20),
              ),
              Expanded(
                child: _buildDrawerContent(),
              ),
            ],
          ),
        ),
      ),
      body: _bodyBuilder(),
//        body: loadingArticle == true
//            ? _buildLoader()
//            : _buildReaderView(
//                context,
//                widget.title != null
//                    ? "${widget.title} - ${this.selectedSideEntry}"
//                    : "",
//                currentResults)
    );
  }

  Widget _bodyBuilder() {
    return StreamBuilder<bool>(
      stream: widget.bloc.isloadingTOC,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data) {
            return _buildLoader();
          } else {
            return StreamBuilder(
              stream: widget.bloc.content,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return _buildReaderView(
                      context,
                      "${widget.title} - ${widget.bloc.currentEntry.name}",
                      snapshot.data);
                }
                return Text("");
              },
            );
          }
        }
        return Container(
          width: 0,
          height: 0,
        );
      },
    );
  }

  Center _buildLoader() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircularProgressIndicator(),
          Padding(
            padding: EdgeInsets.only(bottom: 10),
          ),
          Text("Fetching Content ...")
        ],
      ),
    );
  }

  void fetchResults(Entry e) {
    print("Fetching results ${e.name}");
    print("Fetching results ${e.url}");
    http.fetchPrivateBin(e.url).then((String data) {
      print(data);
      print("setting data now");

      setState(() {
        currentResults = data;
      });

      setState(() {
        loadingArticle = false;
      });
    }).catchError((err) {
      currentResults = err.toString();
    });
  }
}

class TableOfContentViewWidget extends StatelessWidget {
  final sections;
  final TOCBloc bloc;
  final String title;

  TableOfContentViewWidget(this.bloc, this.title, this.sections);

  Widget _buildBody() {
    return ListView.builder(
        itemCount: sections.length,
        itemBuilder: (BuildContext c, int index) {
          List<Widget> ls = [];

          for (var i = 0; i < sections[index].listing.length; i++) {
            ls.add(
              new ListTile(
                leading: i == 0
                    ? CircleAvatar(
                        child: Text(
                            sections[index].listing[i].name[0].toUpperCase()),
                      )
                    : Container(width: 0, height: 0),
                title: Text(
                  sections[index].listing[i].name,
                ),
                onTap: () {
                  this.bloc.currentSection = sections[index];
                  this.bloc.currentSectionIndex = index;
                  openPageRoute(
                      c,
                      BookViewWidget(
                        this.bloc,
                        sections[index].listing[i],
                        title: sections[index].listing[i].name,
                      ));
                },
              ),
            );
          }
          return Column(
            children: ls,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: new Text(this.title),
        ),
        body: _buildBody());
  }
}

