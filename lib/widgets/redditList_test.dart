import 'package:flutter/material.dart';
import 'redit_item.dart';
import 'package:qiunderground/utils/general/types.dart';
import 'package:qiunderground/widgets/redit_item.dart';
import 'package:qiunderground/widgets/providers/provider_home.dart';

class FeedListing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = HomeProvider.of(context);
    return StreamBuilder<List<RedditPost>>(
      initialData: [],
      stream: bloc.posts,
      builder: (context, snapshot) {
        if (snapshot.hasError){
          return Center(
            child: Text("Unable to show feed please try again"),
          );
        }
        if (!snapshot.hasData) {
          return Center(
            child: Column(
              children: <Widget>[
                Text("Nothing to show yet"),
                CircularProgressIndicator(),
              ],
            ),
          );
        }
        return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              if(shouldSkip(snapshot.data[index])){
                return Container();
              }
              return ReditItemCard(snapshot.data[index], bloc);
            });
      },
    );
  }

  bool shouldSkip(RedditPost p ){
    if (p.url.contains("reddit.com")){
      return true;
    }

    if (p.url.contains("redd.it")){
      return true;
    }
    return false;
  }
}
