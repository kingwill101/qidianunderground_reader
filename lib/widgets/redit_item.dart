import 'package:flutter/material.dart';
import 'package:qiunderground/utils/blocs/data_bloc.dart';
import 'package:qiunderground/widgets/reader.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:advanced_share/advanced_share.dart';
import 'package:qiunderground/utils/storage/storage_flutter.dart' as flutil;
import 'package:qiunderground/utils/general/types.dart';

class ReditItem extends StatelessWidget {
  final el;

  ReditItem(this.el);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: ListTile(
            leading: CircleAvatar(
              child: Text(el["data"]["title"].toString()[0]),
            ),
            title: Text(el["data"]["title"]),
            onLongPress: () {
              showModalBottomSheet(
                  builder: (BuildContext ctx) {
                    return Container(
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            title: Text("Add to favorites"),
                          )
                        ],
                        mainAxisSize: MainAxisSize.min,
                      ),
                    );
                  },
                  context: context);
            },
            onTap: () {}));
  }
}

enum ActionType { Save, Hide, Share }

class ReditItemCard extends StatelessWidget {
  final RedditPost el;
  final FeedBloc  bloc;

//  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  ReditItemCard(this.el, this.bloc);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext c) {
          return SingleContentWidget(this.bloc,el.title, el.url);
        }));
      },
      child: Card(
          child: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Row(
              children: <Widget>[
                CircleAvatar(
                  child: new Icon(
                    Icons.web,
                    size: 15.0,
                  ),
                  minRadius: 13,
                ),
                Padding(
                    padding: EdgeInsets.only(left:10.0, right:10.0),
                    child: new Text(
                      "r/QidianUnderground",
                      style: TextStyle(
                        // color: Theme.of(context).primaryTextTheme.caption.color
                      ),
                      )),
                Spacer(),
                PopupMenuButton(
                    icon: Icon(Icons.more_vert),
                    onSelected: (ActionType at) {
                      print(at.toString());
                      switch (at) {
                        case ActionType.Hide:
                          flutil.FlutterStorage().addHidden(el.title, el.url);
                          break;
                        case ActionType.Save:
                          break;
                        case ActionType.Share:
                          AdvancedShare.generic(title:el.title,url: el.url, msg: "Hey check this out!").then((int res){
                              print("$res when trying to share");
                          });
//                        share.Share.share("Hey check out  ${el["data"]["url"]}");
                          break;
                      }
                    },
                    itemBuilder: (BuildContext ctx) =>
                        <PopupMenuItem<ActionType>>[
                          const PopupMenuItem(child: Text("Share"), value: ActionType.Share,),
                          const PopupMenuItem(child: Text("Save"),value: ActionType.Save) ,
                          const PopupMenuItem(child: Text("Hide"), value: ActionType.Hide),
                        ])

                // new Flex(direction: Axis.horizontal, children: <Widget>[Text("Asfsd"), Text("asdfsd") ],),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 2.0),
            ),
            new Center(
              // alignment: Alignment.centerLeft,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      el.title,
                      style: TextStyle(
                          // color: Theme.of(context).primaryTextTheme.caption.color,
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(5.0),
                  ),
                  Row(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                            "Posted by u/${el.authorFullname}",
                            style: TextStyle(
                              // color: Theme.of(context).primaryTextTheme.title.color
                              )
                              ),
                      ),
                      Spacer(),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Text(formatTime(el.createdUtc),
                            style: TextStyle(
                              // color: Theme.of(context).primaryTextTheme.caption.color
                              )
                              ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      )),
    );
  }
}

String formatTime(double time) {
  DateTime dd = new DateTime.fromMillisecondsSinceEpoch(time.toInt() * 1000);
  return timeago.format(dd);
}
