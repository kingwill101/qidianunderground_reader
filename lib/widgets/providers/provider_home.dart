import 'package:flutter/material.dart';
import 'package:qiunderground/utils/blocs/data_bloc.dart';
import 'package:qiunderground/utils/blocs/search_bloc.dart';

class HomeProvider extends InheritedWidget {
  final FeedBloc bloc;
  final SearchBloc searchBloc;

  HomeProvider(this.bloc, this.searchBloc, {Widget child}) : super(child:child);
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  static FeedBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(HomeProvider) as HomeProvider)
        .bloc;
  }

  static SearchBloc search(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(HomeProvider) as HomeProvider)
        .searchBloc;
  }
}
