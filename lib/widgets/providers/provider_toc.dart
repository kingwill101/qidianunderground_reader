import 'package:flutter/material.dart';
import 'package:qiunderground/utils/blocs/toc_bloc.dart';

class TOCProvider extends InheritedWidget {
  final TOCBloc bloc;

  TOCProvider(this.bloc);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  static TOCBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TOCProvider) as TOCProvider)
        .bloc;
  }
}
