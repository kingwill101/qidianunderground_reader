import 'package:flutter/material.dart';


class DataStream extends InheritedWidget{

  final List<dynamic> favs = [];
  final List<dynamic> jsonRes = [];
  DataStream();
  @override
  bool updateShouldNotify(DataStream oldWidget) {
    // oldWidget.favs.,eb
    if ((this.favs.length > oldWidget.favs.length) || this.favs.length > oldWidget.jsonRes.length){
      return true;
    }
    return false;
  }

}