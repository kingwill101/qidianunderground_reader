//import 'package:flutter/material.dart';
//import 'redit_item.dart';
//import 'package:qiunderground/utils/general/types.dart';
//import 'package:qiunderground/utils/http/http.dart';
//
//import 'package:qiunderground/widgets/providers/provider_home.dart';

//class RedditList extends StatefulWidget {
//
//
//  // RedditList();
//  @override
//  RedditListState createState() {
//    return RedditListState();
//  }
//}
//
//class RedditListState extends State<RedditList> {
//  Redditlist jsonRes;
//
//  FeedType _currentSortType;
//
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      children: <Widget>[
//        _showArticleFilterRow(),
//        Expanded(
//          child: Column(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              jsonRes == null ? _showLoader() : _showPostList()
//            ],
//          ),
//        )
//      ],
//    );
//  }
//
//  Widget _showLoader() {
//    return Center(
//      child: Align(
//          alignment: Alignment.center, child: CircularProgressIndicator()),
//    );
//  }
//
//  Widget _showPostList() {
//    return Expanded(
//      child: ListView.builder(
//        itemCount: jsonRes.posts.length,
//        itemBuilder: (BuildContext context, int index) {
//          return ReditItemCard(jsonRes.posts[index]);
//        },
//      ),
//    );
//  }
//
//  Widget _showArticleFilterRow() {
//    return Container(
//      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
//      margin: EdgeInsets.only(right: 0.0),
//      child: Row(
//        crossAxisAlignment: CrossAxisAlignment.center,
//        mainAxisAlignment: MainAxisAlignment.start,
//        mainAxisSize: MainAxisSize.max,
//        children: <Widget>[
//          Padding(
//            child: InkWell(
//                child: Row(children: [
//                  Text(
//                    getFeedType(_currentSortType),
//                    style: TextStyle(fontSize: 17),
//                  ),
//                  Container(),
//                  Icon(Icons.arrow_drop_down)
//                ]),
//                onTap: () => showModalBottomSheet<void>(
//                    builder: (BuildContext c) {
//                      return Column(
//                        mainAxisSize: MainAxisSize.min,
//                        children: <Widget>[
//                          ListTile(
//                              leading: new Icon(Icons.hot_tub),
//                              title: Text("Hot"),
//                              onTap: () {
//                                getContent(FeedType.Hot);
//                                Navigator.pop(c);
//                              }),
//                          ListTile(
//                              leading: new Icon(Icons.new_releases),
//                              title: Text("New"),
//                              onTap: () {
//                                getContent(FeedType.New);
//                                Navigator.pop(c);
//                              }),
//                          ListTile(
//                              leading: new Icon(Icons.trending_up),
//                              title: Text("Top"),
//                              onTap: () {
//                                getContent(FeedType.Top);
//                                Navigator.pop(c);
//                              }),
//                          ListTile(
//                              leading: new Icon(Icons.mail),
//                              title: Text("Controversial"),
//                              onTap: () {
//                                getContent(FeedType.Controversial);
//                                Navigator.pop(c);
//                              }),
//                        ],
//                      );
//                    },
//                    context: context)),
//            padding: EdgeInsets.only(left: 8.0),
//          ),
//          Spacer(
//            flex: 1,
//          ),
//          Flexible(
//            child: new Text(
//              "",
//              style: new TextStyle(fontWeight: FontWeight.bold),
//            ),
//          )
//        ],
//      ),
//    );
//  }
//
//  @override
//  void initState() {
//    super.initState();
//
//
//
//  }
//
//  @override
//  void didChangeDependencies() {
//    super.didChangeDependencies();
//
//    if (jsonRes == null) {
//      getContent(_currentSortType);
//    }
//  }
//
//  void getContent(FeedType ft) {
//    HomeProvider.of(context).sortType.add(_currentSortType);
//
//    if (ft == null) {
//      setState(() {
//        _currentSortType = FeedType.New;
//      });
//    } else if (_currentSortType == ft) {
//      return;
//    } else {
//      setState(() {
//        _currentSortType = ft;
//      });
//    }
//
//    fetchResult(getFeedType(_currentSortType).toLowerCase());
//  }
//
//  fetchResult(String cat) {
//    print("Category - > $cat");
//
//    setState(() {
//      jsonRes = null;
//    });
//
//    fetchCategory(cat).then((r) {
//      setState(() {
//        jsonRes = r;
//      });
//    });
//  }
//}
