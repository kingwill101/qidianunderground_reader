import 'package:flutter/material.dart';
import 'package:qiunderground/screens/home_screen.dart';

//import 'package:qiunderground/screens/qi_index.dart';
import 'package:qiunderground/screens/toc.dart';
import 'package:qiunderground/screens/saved_posts.dart';
import 'package:qiunderground/utils/blocs/search_bloc.dart';
import 'package:qiunderground/utils/general/types.dart';
import 'package:qiunderground/utils/blocs/data_bloc.dart';
import 'package:qiunderground/utils/blocs/toc_bloc_flutter.dart';
import 'package:qiunderground/widgets/providers/provider_home.dart';

const newOrg = "743217d4e79445888f1bbc191d6f48f2";

void main() => runApp(new QiUndergroundAp());

class QiUndergroundAp extends StatefulWidget {
  final feedbloc = FeedBloc();
  final searchBloc = SearchBloc();

  @override
  _QiUndergroundApState createState() => _QiUndergroundApState();
}

class _QiUndergroundApState extends State<QiUndergroundAp> {
  Configuration config = Configuration(darkMode: false);

  void configurationUpdater(Configuration val) {
    setState(() {
      print("updating config");
      config = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Qidian Underground',
      theme: new ThemeData(
          primarySwatch: Colors.blue,
          brightness: config.darkMode ? Brightness.dark : Brightness.light),
      initialRoute: "/",
      routes: {
        '/': (context) => HomeProvider(widget.feedbloc, widget.searchBloc,
            child: HomeScreen(config, configurationUpdater, widget.feedbloc)),
        '/index': (context) => Toc(false, TOCBlocFlutter()),
        '/toc': (context) => Toc(true, QIndexBlocFlutter()),
        '/saved': (context) => SavedPosts(),
//        '/search': (context) => SearchScreen(),
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    widget.feedbloc.cleanup();
  }
}
