import 'dart:io';
import 'package:flutter/material.dart';
import 'package:qiunderground/screens/search_sreen.dart';
import 'package:qiunderground/utils/blocs/data_bloc.dart';
import 'package:qiunderground/widgets/network_reload_widget.dart';
import 'package:qiunderground/widgets/providers/provider_home.dart';
import 'package:qiunderground/utils/general/types.dart';
import 'package:qiunderground/widgets/redit_item.dart';

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  bool darkMode = false;

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();
    return Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text("Qidian Underground"),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.white,
            iconSize: 32,
            onPressed: () => showSearch(
                context: context,
                delegate: QiSearch(HomeProvider.search(context), widget.bloc)),
          )
        ],
      ),
      drawer: _drawer(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _showArticleFilterRow(context),
          Expanded(
            child: _showListing(),
          )
        ],
      ),
//      body: RedditList(),
    );
  }

  Future<bool> usingCache() async {
    var usingCache = await HomeProvider.of(context).usingCache.first;
    return usingCache;
  }

  Widget _showListing() {
    final bloc = HomeProvider.of(context);
    return StreamBuilder<List<RedditPost>>(
      initialData: [],
      stream: bloc.posts,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          ErrorHolder err = snapshot.error;

          if (err.data != null) {
            return _buildListing(err.data);
          }

          if (err.error.runtimeType == SocketException) {
            return NetworkReloadWidget("Network error, make  sure you are connected to the internet", bloc.updateCurrent);
          }
          return Center(
            child: Column(
              children: <Widget>[
                Text("Unable to show feed"),
              ],
            ),
          );
        }

        if (snapshot.hasData) {

          return _buildListing(snapshot.data);
        }

        if (!snapshot.hasData) {
          return Center(
            child: Column(
              children: <Widget>[
                Text("Nothing to show yet"),
              ],
            ),
          );
        }
      },
    );
  }

  Widget _buildListing(d) {
    final bloc = HomeProvider.of(context);
    print("building- ${d.length}");

    if(d.length < 1){
      return NetworkReloadWidget("Feed is empty, click to refresh", bloc.updateCurrent);
    }
    return ListView.builder(
        itemCount: d.length,
        itemBuilder: (context, index) {
          if (d[index].shouldSkip()) {
            return Container(width: 0.0, height: 0.0);
          }
          return ReditItemCard(d[index], widget.bloc);
        });
  }

  void setStream(FeedType ft, BuildContext c) {
    HomeProvider.of(context).sortType.add(ft);
    Navigator.pop(c);
  }

  Widget _drawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: Text('QIDIAN UNDERGROUND',
                  style: Theme.of(context).primaryTextTheme.headline),
            ),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
          ),
          ListTile(
            title: Text('Auto TOC'),
            leading: new Icon(Icons.book),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/toc');
            },
          ),
          ListTile(
            title: Text('TOC Reuploaded'),
            leading: new Icon(Icons.collections_bookmark),
            onTap: () {
              Navigator.pop(context);

              Navigator.pushNamed(context, '/index');
            },
          ),
          ListTile(
            title: Text('Saved Content'),
            leading: new Icon(Icons.bookmark),
            onTap: () {
              Navigator.pop(context);

              Navigator.pushNamed(context, '/saved');
            },
          ),
          ListTile(
            title: Text('Settings'),
            leading: new Icon(Icons.settings),
            onTap: () {
              Navigator.pop(context);

              Navigator.pushNamed(context, '/saved');
            },
          ),
          // Spacer(),
          Divider(),
          ListTile(
              title: Text("Dark Mode"),
              onTap: () => darkModeUpdate(),
              trailing: Switch(
                value: widget.config.darkMode,
                onChanged: (bool value) => darkModeUpdate(),
              ))
        ],
      ),
    );
  }

  Widget _showArticleFilterRow(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
      margin: EdgeInsets.only(right: 0.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            child: InkWell(
                child: Row(children: [
                  StreamBuilder<FeedType>(
                    initialData: FeedType.New,
                    stream: HomeProvider.of(context).getStream,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (!snapshot.hasData) {
                        return FlutterLogo();
                      }
                      return Text(
                        getFeedType(snapshot.data),
                        style: TextStyle(fontSize: 17),
                      );
                    },
                  ),
                  Container(width: 0.0, height: 0.0),
                  Icon(Icons.arrow_drop_down),
                  SizedBox(
                    child: StreamBuilder<bool>(
                      stream: HomeProvider.of(context).isLoading,
                      initialData: false,
                      builder:
                          (BuildContext context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data) {
                            return CircularProgressIndicator();
                          } else {
                            return Center(
                              child: Container(width: 0.0, height: 0.0),
                            );
                          }
                        }
                        return Center(
                          child: Container(width: 0.0, height: 0.0),
                        );
                      },
                    ),
                  ),
                  Align(
                      alignment: Alignment.centerRight,
                      child: StreamBuilder<bool>(
                        stream: HomeProvider.of(context).usingCache,
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            if (snapshot.data) {
                              return Text("showing cache");
                            } else {
                              return Container(width: 0.0, height: 0.0);
                            }
                          } else {
                            return Container(
                              width: 0.0,
                              height: 0.0,
                            );
                          }
                        },
                      ))
                ]),
                onTap: () => showModalBottomSheet<void>(
                    builder: (BuildContext c) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                              leading: new Icon(Icons.hot_tub),
                              title: Text("Hot"),
                              onTap: () => setStream(FeedType.Hot, c)),
                          ListTile(
                              leading: new Icon(Icons.new_releases),
                              title: Text("New"),
                              onTap: () => setStream(FeedType.New, c)),
                          ListTile(
                              leading: new Icon(Icons.trending_up),
                              title: Text("Top"),
                              onTap: () => setStream(FeedType.Top, c)),
                          ListTile(
                              leading: new Icon(Icons.mail),
                              title: Text("Controversial"),
                              onTap: () =>
                                  setStream(FeedType.Controversial, c)),
                        ],
                      );
                    },
                    context: context)),
            padding: EdgeInsets.only(left: 8.0),
          ),
          Spacer(
            flex: 1,
          ),
          Flexible(
            child: new Text(
              "",
              style: new TextStyle(fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }

  void darkModeUpdate() {
    widget.config.darkMode
        ? widget.updater(Configuration(darkMode: false))
        : widget.updater(Configuration(darkMode: true));
  }

}

class HomeScreen extends StatefulWidget {
  final ValueChanged<Configuration> updater;
  final Configuration config;
  final FeedBloc  bloc;

  HomeScreen(this.config, this.updater, this.bloc);

  @override
  _HomeScreenState createState() {
    return _HomeScreenState();
  }
}
