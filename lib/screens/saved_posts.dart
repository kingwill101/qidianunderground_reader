import 'package:flutter/material.dart';
import 'package:qiunderground/utils/util.dart';
import 'package:flukit/flukit.dart';
import 'package:qiunderground/utils/utils_flutter.dart';
import 'package:qiunderground/widgets/reader.dart';
import 'package:qiunderground/utils/general/types.dart';
import 'package:qiunderground/utils/http/http.dart' as http;


class SavedPosts extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SavedPosts();
  }
}

class _SavedPosts extends State<SavedPosts> {
  List<Section> sections = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: new Text("Saved Posts"),
        ),
        body: sections.length <1? Center(child: CircularProgressIndicator(),): QuickScrollbar(
          child: ListView.builder(
              itemCount: sections.length,
              itemBuilder: (BuildContext c, int index) {
                List<Widget> ls = [];

                for (var i = 0; i < sections[index].listing.length; i++) {
                  ls.add(
                    new ListTile(
                      leading: i == 0
                          ? CircleAvatar(
                              child: Text(sections[index]
                                  .listing[i]
                                  .name[0]
                                  .toUpperCase()),
                            )
                          : null,
                      title: Text(
                        sections[index].listing[i].name,
                      ),
                      trailing: new Icon(Icons.arrow_forward),
                      onTap: () => openPageRoute(c, BookViewWidget(null, sections[index].listing[i])),
                    ),
                  );
                }
                return Column(
                  children: ls,
                );
              }),
        ));
  }

  @override
  void initState() {
    super.initState();

    if (sections.length < 1) {
      print("fetching index");
     http.getQIndex().then((String res) {
        buildListing(res).then((List<Section> l) {
          print(res);
          setState(() {
            sections = l;
          });
        });
      });
    }
  }

}


