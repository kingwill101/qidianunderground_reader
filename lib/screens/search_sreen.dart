import 'package:flutter/material.dart';
import 'package:qiunderground/utils/blocs/data_bloc.dart';
import 'package:qiunderground/utils/blocs/search_bloc.dart';
import 'package:qiunderground/utils/general/types.dart';
import 'package:qiunderground/widgets/redit_item.dart';

class QiSearch extends SearchDelegate<List<RedditPost>> {
  final SearchBloc searchBloc;
  final FeedBloc bloc;

  FeedType searchType = FeedType.New;

  QiSearch(this.searchBloc, this.bloc);

  void setStream(FeedType ft, BuildContext context) {
    searchType = ft;
    Navigator.pop(context);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
          icon: Icon(Icons.settings),
          onPressed: () {
            showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                          leading: new Icon(Icons.hot_tub),
                          title: Text(
                            "Hot",
                          ),
                          trailing: searchType == FeedType.Hot
                              ? Icon(Icons.check_box)
                              : Container(width: 0.0, height: 0.0,),
                          onTap: () => setStream(FeedType.Hot, context)),
                      ListTile(
                          leading: new Icon(Icons.new_releases),
                          title: Text(
                            "New",
                          ),
                          trailing: searchType == FeedType.New
                              ? Icon(Icons.check_box)
                              : Container(width: 0.0, height: 0.0,),
                          onTap: () => setStream(FeedType.New, context)),
                      ListTile(
                          leading: new Icon(Icons.trending_up),
                          title: Text(
                            "Top",
                          ),
                          trailing: searchType == FeedType.Top
                              ? Icon(Icons.check_box)
                              : Container(width: 0.0, height: 0.0,),
                          onTap: () => setStream(FeedType.Top, context)),
                      ListTile(
                          leading: new Icon(Icons.mail),
                          title: Text(
                            "Controversial",
                          ),
                          trailing: searchType == FeedType.Controversial
                              ? Icon(Icons.check_box)
                              : Container(width: 0.0, height: 0.0,),
                          onTap: () =>
                              setStream(FeedType.Controversial, context)),
                    ]);
              },
            );
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () => close(context, null),
      icon: Icon(Icons.arrow_back),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    searchBloc.query.add(SearchHolder(
        this.query, {"sort": getFeedType(searchType).toLowerCase()}));
    return StreamBuilder(
      stream: searchBloc.results,
      builder:
          (BuildContext context, AsyncSnapshot<List<RedditPost>> snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Text(
                "Unable to proceed, please check your internet connection"),
          );
        }
        if (snapshot.hasData) {
          if (!snapshot.hasError) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return ReditItemCard(snapshot.data[index], bloc);
                });
          } else {
            return Container(width: 0.0, height: 0.0);
          }
        } else {
          return Container(width: 0.0, height: 0.0);
        }
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return Container(width: 0.0, height: 0.0);
  }
}

//class SearchScreen extends StatefulWidget {
//  @override
//  _SearchScreenState createState() => _SearchScreenState();
//}
//
//class _SearchScreenState extends State<SearchScreen> {
//  final FlutterStorage fs = FlutterStorage();
//  Redditlist searchResults;
//  TextEditingController _searchController = TextEditingController();
//
//  String lastQuery;
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//        appBar: AppBar(
//          title: TextField(
//            cursorColor: Colors.white,
//            enabled: true,
//            controller: _searchController,
//            keyboardType: TextInputType.text,
//            style: TextStyle(
//                color: Theme.of(context).primaryTextTheme.body1.color),
//            decoration: InputDecoration(
//                disabledBorder: InputBorder.none,
//                border: InputBorder.none,
//                hintText: "Search r/QidianUndeground",
//                hintStyle: TextStyle(
//                    color: Theme.of(context).primaryTextTheme.caption.color),
//                suffixIcon: new Padding(
//                  child: Icon(Icons.search),
//                  padding: EdgeInsets.only(right: 12.0),
//                )),
//            onChanged: (s) {
//              print("changed");
//              var currentQuery = s.trim();
//              if (lastQuery == currentQuery) {
//                return;
//              }
//
//              lastQuery = currentQuery;
//
//              if (currentQuery.length >= 3) {
//                http.searchQU(s).then((res) {
//                  setState(() {
//                    searchResults = res;
//                  });
//                  print(res);
//                }).catchError((err) {
//                  print("SEARCH ERROR");
//                });
//              }
//              // searchQuery = s;
//            },
//          ),
//          // actions: <Widget>[
//          //   IconButton(
//          //     icon: Icon(Icons.search),
//          //     onPressed: () {
//          //       print(_searchController.text);
//          //     },
//          //   )
//          // ],
//        ),
//        body: Column(
//          children: <Widget>[
//            searchResults != null
//                ? _buildSearchResults()
//                : Container(child: Text("Nothing to show yet"))
//          ],
//        ));
//  }
//
//  Widget _buildSearchResults() {
//    return Expanded(
//      child: ListView.builder(
//        itemCount: searchResults?.posts?.length,
//        itemBuilder: (BuildContext context, int index) {
//          return ReditItemCard(searchResults?.posts[index], );
//        },
//      ),
//    );
//  }
//
//  @override
//  void initState() {
//    super.initState();
//  }
//}
//
//class SearchBox extends StatelessWidget {
//  const SearchBox({
//    Key key,
//  }) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      // decoration: BoxDecoration(color: Colors.white),
//      child: Row(
//        children: <Widget>[
//          Container(
//            child: Icon(
//              Icons.search,
//              // color: Colors.grey,
//            ),
//            margin: EdgeInsets.only(right: 10),
//          ),
//          Expanded(
//            child: TextField(
//                // style: TextStyle(color: Colors.black),
//                ),
//          ),
//          IconButton(
//            icon: Icon(
//              Icons.close,
//            ),
//            onPressed: () {},
//          )
//        ],
//      ),
//    );
//  }
//}
