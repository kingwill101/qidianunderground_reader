import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qiunderground/utils/blocs/toc_bloc.dart';
import 'package:qiunderground/utils/general/general.dart';
import 'package:qiunderground/widgets/network_reload_widget.dart';
import 'package:qiunderground/widgets/reader.dart';

class Toc extends StatefulWidget {
  final TOCBloc bloc;

  final bool autotc;

  String title;

  Toc(this.autotc, this.bloc) {
    if (autotc) {
      this.title = "Auto TOC";
    } else {
      this.title = "RE  TOC";
    }
  }

  @override
  State<StatefulWidget> createState() {
    return _TocState();
  }
}

class _TocState extends State<Toc> {
  Widget _getContentView(List<Section> data) {
    widget.bloc.sections = data;
    return TableOfContentViewWidget(widget.bloc, widget.title, data);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[

      StreamBuilder<List<Section>>(
        stream: widget.bloc.sections,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            ErrorHolder err = snapshot.error;

            if (err.data != null) {
              return _getContentView(err.data);
            }

            if (err.error.runtimeType == SocketException) {
              return Scaffold(
                  appBar: AppBar(
                    title: Text(widget.title),
                  ),
                  body: NetworkReloadWidget(
                      "Network error, make  sure you are connected to the internet",
                      widget.bloc.updateFunc));
            }
            return Center(
              child: Column(
                children: <Widget>[
                  Text("Unable to show feed, try again later"),
                ],
              ),
            );
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (snapshot.hasData) {
              return _getContentView(snapshot.data);
            } else {
              return Center(
                child: Column(
                  children: <Widget>[
                    Text("Unable to show feed, try again later"),
                  ],
                ),
              );
            }
          }
        },
      ),
      StreamBuilder<bool>(
        stream:widget.bloc.isloadingTOC,
        builder: (
            BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData){
            if(snapshot.data){
              return Center(
                child: CircularProgressIndicator(),
              );
            }else{
              return Container(height: 0, width: 0,);
            }
          }
          return Container(height: 0, width: 0,);
        },),
    ],);
  }

  @override
  void initState() {
    super.initState();
    widget.bloc.updateSections();
  }
}
