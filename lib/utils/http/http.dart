import 'dart:convert';
import 'package:http/http.dart';
import 'dart:async';
import 'package:qiunderground/utils/general/types.dart';

Future<String> getQIndex() async {
  Completer c = Completer<String>();

  get("https://www.reddit.com/r/QidianUnderground/wiki/toc_reup.json")
      .then((Response res) {
    try {
      var bod = jsonDecode(res.body);
      String data = bod['data']['content_html'];
      c.complete(data);
    } catch (e) {
      c.completeError(e);
    }
  }).catchError((err) {
    c.completeError(err);
  });

  return c.future;
}

Future<String> getToc() async {
  Completer c = Completer<String>();
  get("https://www.reddit.com/r/QidianUnderground/wiki/autotoc.json")
      .then((Response res) {
    try {
      print(res.body);

      var bod = jsonDecode(res.body);
      String data = bod['data']['content_html'];
      c.complete(data);
    } catch (e) {
      c.completeError(e);
    }
  }).catchError((err) {
    c.completeError(err);
  });

  return c.future;
}

Future<Redditlist> searchQU(String query, Map<String, dynamic> args) {
  Completer<Redditlist> c = Completer<Redditlist>();

  Map<String, dynamic> params = {"q": query, "restrict_sr": "1"};
  params.addAll(args);

  Uri outgoingUri = new Uri(
      scheme: 'https',
      host: 'reddit.com',
      path: 'r/QidianUnderground/search.json',
      queryParameters: params);
  get(outgoingUri).then((Response r) {
    try {
      Redditlist res = Redditlist.fromJson(r.body);

      c.complete(res);
    } catch (er) {
      print("Error parsing - > $er");
      c.completeError(er);
    }
  }).catchError((err) {
    c.completeError(err);
  });

  return c.future;
}

Future<Redditlist> fetchCategory(String cat) {
  Completer<Redditlist> c = Completer();
  print("Category - > $cat");

  String url = "https://www.reddit.com/r/QidianUnderground/$cat.json";

  get(url).then((Response r) {
    Redditlist res = Redditlist.fromJson(r.body);

    c.complete(res);
  }).catchError((e) {
    c.completeError(e);
  });

  return c.future;
}

Future<PrivateBinInfo> fetchPrivateBin(String url) {
  Completer c = Completer<PrivateBinInfo>();

  String pw = url.split("#")[1];

  get("$url&json", headers: {'X-Requested-With': 'JSONHttpRequest'})
      .then((Response r) async {
    c.complete(PrivateBinInfo(json.decode(r.body)['data'], pw));
  }).catchError((Object err) {
    print("error fetching $url ${err.toString()}");
    c.completeError(err);
  });

  return c.future;
}
