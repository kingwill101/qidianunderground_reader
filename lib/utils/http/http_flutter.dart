import 'package:qiunderground/utils/http/http.dart' as http;
import 'package:qiunderground/utils/util.dart' as util;
import 'package:privatebin_reader/privatebin_reader.dart';
import 'dart:async';
import 'package:qiunderground/utils/general/general.dart';

Future<String> fetchPrivateBin(String url) async {
  Completer c = Completer<String>();

  http.fetchPrivateBin(url).then((PrivateBinInfo pi) async {
    print("Decrypting");
    var val = await PrivatebinReader.decrypt(pi.data, pi.password);
    print("decompressing");
    var d = await PrivatebinReader.decompress(val);
    print("all done");
    c.complete(util.cleanString(d));
  }).catchError((err) {
    c.completeError(err);
  });
  return c.future;
}
