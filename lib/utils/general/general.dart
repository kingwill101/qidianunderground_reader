library general;

export 'types.dart';
import 'package:flutter_localstorage/flutter_localstorage.dart' as ls;


const _toc = "toc";
const _index = "index";

class AppState {

  Future<bool> _save(String name, String val) {

    try {
      ls.LocalStorage().setItem(name, val);
      return Future.value(true);
    }
    catch(err){
      Future.error(err);
    }

    return Future.value(false);
  }

  Future<String> _get(String name){
    try{
      return Future.value(ls.LocalStorage().getItem(name));
    }catch(err){
      return Future.error(err);
    }
  }

  Future<bool> saveToc(String data){
    return _save(_toc, data);
  }

  Future<bool> saveIndex(String data){
    return _save(_index, data);
  }

  Future<String> getToc(){
    return _get(_toc);
  }

  Future<String> getIndex(){
    return _get(_index);
  }

}

abstract class Repository {

}