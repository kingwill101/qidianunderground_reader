export 'redit_model.dart';

///
///  Whether Section A, B, C...
///
class Section {
  ///
  ///  name represented by letter of the alphabet
  String name;
  ///
  /// holding each book that falls under the current alphabet
  List<Listing> listing = [];
}
///
/// Listing represents a book holding entries
class Listing {
  ///
  /// name of the book
  String name;
  ///
  /// List of Chapters available in the book
  List<Entry> entries = [];
}

///
/// Representation of a book chapter
class Entry {
  ///
  ///Name of the chapter
  ///
  String name;
  ///
  /// URL to private bin url where chapter is stored
  String url;

  Entry(this.name, this.url);

  @override
  String toString() {
    return "${this.name} - ${this.url}";
  }
}

///
/// As the name states class is used to store errors as well as possible
/// additional data that may be needed to shown alongside the errors
///
class ErrorHolder {
  final data;
  final error;

  ErrorHolder(this.error,  this.data);
}

class PrivateBinInfo{
  final String data;

  final String password;

  PrivateBinInfo(this.data, this.password);
}


enum FeedType { Hot, New, Top, Controversial }

String getFeedType(FeedType ft) {
  switch (ft) {
    case FeedType.Controversial:
      return "Controversial";

    case FeedType.Hot:
      return "Hot";

    case FeedType.New:
      return "New";

    case FeedType.Top:
      return "Top";
    default:
      return "";
  }
}


class Configuration {
  bool darkMode;
  Configuration({this.darkMode=false});
}

class SearchHolder {
  final Map<String, dynamic> params;
  final String query;

  SearchHolder(this.query, [this.params]);
}