import 'package:flutter/material.dart';

void openPageRoute(BuildContext c, Widget widg) {
  Navigator.of(c).push(MaterialPageRoute(builder: (BuildContext c) {
    return widg;
  }));
}

