import 'package:html_unescape/html_unescape.dart';
import 'package:html/parser.dart';
import 'package:html/dom.dart';
import 'dart:async';
import 'general/types.dart';

class HtmlFixResult {
  List<String> keys = [];
  String data;

  HtmlFixResult(this.keys, this.data);
}

HtmlFixResult fixHTML(String data) {
  String res = HtmlUnescape().convert(data).toString();

  List<String> keysFound = [];
  for (var i = 0; i < 26; i++) {
    int currentKeyCode = 97 + i;

    String currKey = String.fromCharCode(97 + i);
    String patternBefore = '<h5 id="wiki_$currKey">';
    String patternAfter = '<div id="wiki__$currKey"> <h5 id="wiki_$currKey">';

    if (currentKeyCode > 97) {
      if (currentKeyCode < 122) {
        patternAfter =
            '</div> <div id="wiki__$currKey"> <h5 id="wiki_$currKey">';
      }
    }

    if (res.contains(patternBefore) == true) {
      res = res.replaceAll(patternBefore, patternAfter);
      keysFound.add("wiki__$currKey");
    }
  }
  res = res + "</div>";

  print("================\n\n $res \n\n==================");
  return HtmlFixResult(keysFound, res);
}

Future<List<Section>> buildListing(String data) async {
  HtmlFixResult res = fixHTML(data);
  Document doc = parse(res.data);
  List<Section> sections = [];
  res.keys.forEach((String key) {
    Section section = Section();

    Element el = doc.getElementById(key);
    if (el != null) {
      var sectionHeading = el.getElementsByTagName("h5")[0].text;

      var li = el.getElementsByTagName("ul");
      var name = el.getElementsByTagName("p");

      section.name = sectionHeading;

      assert(li.length == name.length);

      for (var i = 0; i < li.length; i++) {
        Listing _listing = Listing();
        _listing.name = name[i].text;

        li[i].children[0].children.forEach((Element e) {
          String url = e.attributes.containsKey("href")
              ? e.attributes["href"]
              : "No URL HERE";
          _listing.entries.add(Entry(e.text, url));
        });

        section.listing.add(_listing);
      }
    }
    sections.add(section);
  });

  return sections;
}

String cleanString(String _in) {
  return HtmlUnescape().convert(_in).toString();
}
