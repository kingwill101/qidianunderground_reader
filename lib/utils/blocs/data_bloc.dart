import 'package:qiunderground/utils/http/http.dart';
import 'package:qiunderground/utils/general/types.dart';
import 'dart:async';
import 'package:rxdart/rxdart.dart' show BehaviorSubject;
import 'package:qiunderground/utils/http/http_flutter.dart' as flutter_http;

class FeedBloc {
  final _currentArticleSubject = BehaviorSubject<String>();

  final _currentArticleUrlSubject = BehaviorSubject<String>();

  final _sortTypeController = StreamController<FeedType>();

  final _postsSubject = BehaviorSubject<List<RedditPost>>();

  final _sortTypeSubject = BehaviorSubject<FeedType>(seedValue: FeedType.New);

  final _isloadingSubject = BehaviorSubject<bool>(seedValue: false);

  final _usingCache = BehaviorSubject<bool>(seedValue: false);

  Map<FeedType, Redditlist> _cache = {};

  FeedBloc() {
    _sortTypeController.stream.listen((FeedType ft) {
      if (getFeedType(ft) == getFeedType(_sortTypeSubject.value)) {
        return;
      }

      if (ft != null) {
        _sortTypeSubject.add(ft);
        _fetchFeed();
      }
    });

    _fetchFeed();
  }

  Stream<List<RedditPost>> get posts => _postsSubject.stream;

  Sink<FeedType> get sortType => _sortTypeController.sink;

  Stream<FeedType> get getStream => _sortTypeSubject.stream;

  Stream<bool> get isLoading => _isloadingSubject.stream;

  Stream<bool> get usingCache => _usingCache.stream;

  Future<void> updateCurrent() => _fetchFeed();

  Sink<String> get currentArticleUrl => _currentArticleUrlSubject.sink;

  Stream<String> get currentArticle => _currentArticleSubject.stream;

  void _addToCache(FeedType ft, Redditlist l) {
    _cache[ft] = l;
  }

  Redditlist _getFromCache(ft) {
    if (_cache.containsKey(ft)) {
      print("found $ft in cache");
      return _cache[ft];
    }
    return null;
  }

  _fetchFeed() {
    FeedType _st = _sortTypeSubject.value;
    _isloadingSubject.add(true);

    fetchCategory(getFeedType(_sortTypeSubject.value).toLowerCase()).then((r) {
      _addToCache(_st, r);
//      _usingCache.add(false);
      _postsSubject.add(_getFromCache(_st).posts);
      _isloadingSubject.add(false);
    }).catchError((err) {
      print("DataBloc  ->  network $err");
      var _c = _getFromCache(_st);

      if (_c != null) {
        _postsSubject.add(_c.posts);
        _postsSubject.addError(ErrorHolder(err, _c.posts));
      } else {
        _postsSubject.addError(ErrorHolder(err, null));
      }

      _isloadingSubject.add(false);
    });
  }

  updateCurrentArticle() {

//    _currentArticleSubject.add("");

    flutter_http.fetchPrivateBin(_currentArticleUrlSubject.value).then((str) {
      _currentArticleSubject.add(str);
    }).catchError((err) {
      _currentArticleSubject.addError(err);
    });
  }

  void cleanup() {
    _sortTypeSubject.close();
    _isloadingSubject.close();
    _sortTypeController.close();
    _usingCache.close();
    _currentArticleSubject.close();
    _currentArticleUrlSubject.close();
  }
}
