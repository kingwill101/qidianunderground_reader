import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'package:qiunderground/utils/general/types.dart';

abstract class TOCBloc {
  final BehaviorSubject<List<Section>> _sections =
      BehaviorSubject<List<Section>>(seedValue: []);
  final BehaviorSubject<Listing> _currentListing = BehaviorSubject<Listing>();
  final BehaviorSubject<int> _currentListingIndex = BehaviorSubject<int>();

  final BehaviorSubject<Entry> _currentEntry = BehaviorSubject<Entry>();
  final BehaviorSubject<int> _currentEntryIndex =
      BehaviorSubject<int>(seedValue: 0);
  final BehaviorSubject<Section> _currentSection = BehaviorSubject<Section>();
  final BehaviorSubject<int> _currentSectionIndex = BehaviorSubject<int>();
  final BehaviorSubject<bool> _loadingArticle = BehaviorSubject<bool>();
  final BehaviorSubject<String> _currentStoryContent =
      BehaviorSubject<String>();

  final  BehaviorSubject<bool> _loadingToc =  BehaviorSubject<bool>(seedValue: false);


  final StreamController<int> _entryChanger = StreamController<int>();

  TOCBloc() {
    _entryChanger.stream.listen((int index) {
      _loadingArticle.add(true);
      _currentEntryIndex.add(index);
      _currentEntry
          .add(_currentListing.value.entries[_currentEntryIndex.value]);
      readPrivateBin().then((v) {
        _loadingArticle.add(false);
      });
    });
  }

  updateSections();


  void setLoadingState(bool s){
    _loadingArticle.add(s);
  }

  Future<void> updateFunc() => updateSections();

  void addSection(List<Section> s) {
    this._sections.add(s);
  }

  void addSectionErr(ErrorHolder e) {
    this._sections.addError(e);
  }

  updateListings() {}

  Stream<bool> get  isloadingTOC => _loadingArticle.stream;

  Sink<List<Section>> get addSec => _sections.sink;

  Sink<int> get entryIndexChanger => _entryChanger.sink;

  Future<void> readPrivateBin();

  Stream<String> get content => _currentStoryContent.stream;

  Entry get currentEntry => _currentEntry.value;

  Stream<Listing> get listing => _currentListing.stream;

  set currentSection(Section s) => _currentSection.add(s);
  set  currentSectionIndex(int index) => _currentSectionIndex.add(index);
  set  sections(List<Section> s) => _sections.add(s);
  get sections =>  _sections.stream;

  void updateContent(String c) {
    _currentStoryContent.add(c);

  }

  void updateContentError(Object err) {
    _currentStoryContent.addError(ErrorHolder(err, null));
  }

  void setCurrentListing(Listing l) {
    _currentListing.add(l);
  }

  void cleanup() {
    _sections.close();
    _currentEntry.close();
    _currentEntryIndex.close();
    _currentSection.close();
    _currentSectionIndex.close();

    _loadingArticle.close();
    _currentStoryContent.close();
    _currentListing.close();
    _currentListingIndex.close();
    _entryChanger.close();
  }
}
