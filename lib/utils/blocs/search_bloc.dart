import 'dart:async';
import 'package:qiunderground/utils/general/redit_model.dart';
import 'package:qiunderground/utils/general/types.dart';
import 'package:rxdart/rxdart.dart';
import 'package:qiunderground/utils/http/http.dart' as http;

class SearchBloc {
  StreamController<SearchHolder> _queryController =
      StreamController<SearchHolder>();

  String _lastQuery = "";
  Map<String, dynamic> _lastParam;

  BehaviorSubject<List<RedditPost>> _results =
      BehaviorSubject<List<RedditPost>>();

  SearchBloc() {
    _queryController.stream.listen((SearchHolder q) {
      doSearch(q);
    });
  }

  Sink<SearchHolder> get query => _queryController.sink;

  Stream<List<RedditPost>> get results => _results.stream;

  void cleanUp() {
    _queryController.close();
    _results.close();
  }

  void doSearch(SearchHolder q) {
    print("Search query -> ${q.query}");

    if (_lastParam != null) {
      if (q.query == _lastQuery) {
        print("already searched for ${q.query}");

        bool foundDifference = false;

        q.params.keys.forEach((key) {
          if (_lastParam.containsKey(key)) {
            if (_lastParam[key] != q.params[key]) {
              foundDifference = true;
            }
          } else {
            foundDifference = true;
          }
        });

        if (!foundDifference) {
          return;
        }
        print("Found a difference");
      }
    }

    _lastQuery = q.query;

    http.searchQU(q.query, q.params).then((res) {
      _results.add(res.posts);
    }).catchError((err) {
      _results.add([]);
      _results.addError(err);
    });
  }
}
