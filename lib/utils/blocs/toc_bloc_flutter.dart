import 'dart:async';

import 'package:qiunderground/utils/http/http.dart' as http;
import 'package:qiunderground/utils/general/types.dart';
import 'package:qiunderground/utils/util.dart' as utils;
import 'package:qiunderground/utils/blocs/toc_bloc.dart';
import 'package:qiunderground/utils/http/http_flutter.dart';

class TOCBlocFlutter extends TOCBloc {
  @override
  Future<void> readPrivateBin() {
    setLoadingState(true);
    Completer<void> c = Completer<void>();

    fetchPrivateBin(currentEntry.url).then((res) {
      updateContent(res);
      setLoadingState(false);
      c.complete();
    }).catchError((err) {
      updateContentError(err);
      setLoadingState(false);
      c.completeError(err);
    });
    return c.future;
  }

  @override
  updateSections() {
    setLoadingState(true);
    http.getToc().then((String res) {
      utils.buildListing(res).then((List<Section> l) {
        addSection(l);
        setLoadingState(false);
      }).catchError((err) {
        addSectionErr(ErrorHolder(err, null));
        setLoadingState(false);
      });
    }).catchError((err) {
      addSectionErr(ErrorHolder(err, null));
      setLoadingState(false);
    });
  }
}

class QIndexBlocFlutter extends TOCBloc {
  @override
  updateSections() {
    setLoadingState(true);
    http.getQIndex().then((String res) {
      utils.buildListing(res).then((List<Section> l) {
        addSection(l);
        setLoadingState(false);
      }).catchError((err) {
        addSectionErr(ErrorHolder(err, null));
        setLoadingState(false);
      });
    }).catchError((err) {
      addSectionErr(ErrorHolder(err, null));
      setLoadingState(false);
    });
  }

  @override
  Future<void> readPrivateBin() {
    Completer<void> c = Completer<void>();
    setLoadingState(true);
    fetchPrivateBin(currentEntry.url).then((res) {
      updateContent(res);
      setLoadingState(false);
      c.complete();
    }).catchError((err) {
      updateContentError(err);
      setLoadingState(false);
      c.completeError(err);
    });
    return c.future;
  }
}
