import 'dart:async';
import 'package:qiunderground/utils/storage/storage.dart';
import 'package:sqflite/sqflite.dart';

class FlutterStorage implements StorageClass {
  Database _db;

  FlutterStorage() {
    _getDatabase().then(((d) {
      this._db = d;
    }));
  }

  Future<List<Map<String, dynamic>>> getSearchResults() async {
    Completer<List<Map<String, dynamic>>> c = Completer();
      c.complete(_db.rawQuery("SELECT * FROM search"));
    return c.future;
  }

  Future<List<Map<String, dynamic>>> getHidden() async {
    Completer<List<Map<String, dynamic>>> c = Completer();
    c.complete(_db.rawQuery("SELECT * FROM hidden"));

    return c.future;
  }

  Stream<void> addHidden(String name, String uri) {
    Completer<Stream<void>> c = Completer<Stream<void>>();

    c.complete(_db.execute(
        "INSERT INTO hidden(name, uri) VALUES(?,?)", [name, uri]).asStream());

    return c.future.asStream();
  }

  Stream<void> saveSearchQuery(String query) {
    Completer<Stream<void>> c = Completer<Stream<void>>();
    c.complete(
        _db.execute("INSERT INTO search(query) VALUES(?)", [query]).asStream());
    return c.future.asStream();
  }

  Future<Database> _getDatabase() async {
    String dpath = await getDatabasesPath();
    String path = "$dpath/qiunderground.db";
    Database database = await openDatabase(path, version: 1,
        onCreate: (Database _db, int version) async {
          // When creating the db, create the table
          await _db.execute(
              'CREATE TABLE IF NOT EXISTS saved (id INTEGER PRIMARY KEY, name TEXT, value INTEGER, num REAL)');
          await _db.execute(
              'CREATE TABLE IF NOT EXISTS Test (id INTEGER PRIMARY KEY, name TEXT, value INTEGER, num REAL)');
          await _db.execute(
              'CREATE TABLE IF NOT EXISTS search (id INTEGER PRIMARY KEY, query TEXT)');
          await _db.execute(
              'CREATE TABLE IF NOT EXISTS hidden (id INTEGER PRIMARY KEY, name TEXT, uri TEXT)');
        });
    return database;
  }

}
