abstract class StorageClass {
  Future<List<Map<String, dynamic>>> getSearchResults();

  Future<List<Map<String, dynamic>>> getHidden();

  Stream<void> addHidden(String name, String uri);

  Stream<void> saveSearchQuery(String query);
}

abstract class KeyStore {
  Stream<T> getKey<T>(String name);

  Stream<void> setKey<T>(String name, T item);
}
